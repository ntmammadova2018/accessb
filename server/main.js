import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.publish("allPlaylists", function(){
	if (this.userId) {
		return Playlists.find({userId: this.userId});
	}
});

Meteor.publish("allSingers", function(){
	return Singers.find({});
});

Meteor.publish("allSongs", function(){
	return Songs.find({});
});

Meteor.publish("playlistSongs", function(id){
	return Playlists.find({_id: id});
});

Meteor.publish("specificSongs", function(playlistId){
	let playlist = Playlists.findOne({_id: playlistId});
	if(playlist && playlist.song){  	
      let songs = playlist.song;

      return Songs.find({_id: {$in: songs}});
  	}
});

Meteor.publish("specificSingers", function(playlistId){
	let playlist = Playlists.findOne({_id: playlistId});
	if(playlist && playlist.song){  	
      let songs = playlist.song;
      let singers = songs.map(function(songId){
          let singersArray = [];
          if(Songs.findOne({_id: songId})){
		    let song = Songs.findOne({_id: songId});
            return song.singer;             
          }          
      });

      return Singers.find({_id: {$in: singers}});
  	}
});

