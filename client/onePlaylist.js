import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.onePlaylist.onCreated(function OnCreated() {
  // reactive var for found songs
  this.foundSong = new ReactiveVar();
});

Template.onePlaylist.helpers({
  playlistID(){
    const playlistId = Template.instance().data;
    let id = new Mongo.ObjectID(playlistId);
  	let playlist = Playlists.findOne({_id: id});

  	if(playlist)
  		return playlist._id._str; 	
    },
  playlistName(){
    const playlistId = Template.instance().data;
    let id = new Mongo.ObjectID(playlistId);
    let playlist = Playlists.findOne({_id: id});
    if(playlist)
      return playlist.name;
  },
  ////returns title and singerName as object for each song
  playlistSongs(){
	  const playlistId = Template.instance().data;	 
      let id = new Mongo.ObjectID(playlistId);
      let playlist = Playlists.findOne({_id: id});    
      	if(playlist && playlist.song){  	
          let songs = playlist.song;		
          let songData = songs.map(function(songId){
              let songObj = {};
              songObj["listId"] = playlist._id._str;
              if(Songs.findOne({_id: songId})){
    		    let song = Songs.findOne({_id: songId});				
                songObj["_id"] = song._id;
                songObj["title"] = song.title;
                let singerId = song.singer; 

                if(Singers.findOne({_id: singerId}))
                  songObj["singerName"] = Singers.findOne({_id: singerId}).name;    				
               return songObj;
              }
          });
		  
          return songData;
		}
	},
  found(){
	  let id = Template.instance().foundSong.get();
	  return (id !== undefined && id !== "undefined");
  },
  foundSong(){
	  let songObj = {};
	  const playlistId = Template.instance().data;	 
      let id = new Mongo.ObjectID(playlistId);
      let playlist = Playlists.findOne({_id: id});
	  songObj["listId"] = playlist._id._str;
	  let foundSong = Template.instance().foundSong.get();
		console.log(foundSong);
	  if(Songs.findOne({_id: foundSong})){
		let song = Songs.findOne({_id: foundSong});				
		songObj["_id"] = song._id;
		songObj["title"] = song.title;
		let singerId = song.singer; 

		if(Singers.findOne({_id: singerId}))
		  songObj["singerName"] = Singers.findOne({_id: singerId}).name; 

	   return songObj;
	  }
  }

});

Template.onePlaylist.events({
  'submit #searchForm' :(event,instance) => {
    const inputData = $('#inputSearch').val();	
	let id = "undefined";
	if(inputData !== undefined && inputData !== ""){
		let song = Songs.findOne({title:{$regex: inputData, $options:'i'}});
		if(song)
		id = song._id;
	}
	else{
		id = "undefined";
	}
	instance.foundSong.set(id);
    return false;
  }
});

Template.oneSong.events({
	'click .remove-icon':(event) => {
		const songId = event.target.id;
		const playlistId = $(event.target).attr('name');
		let id = new Mongo.ObjectID(playlistId);
		return Playlists.update({_id: id}, {$pull: {song: songId}});
	}
});

Template.addSong.onCreated(function () {
  this.playlistId = "";
});

Template.addSong.events({
  'submit #addForm' :(event,instance) => {
		let songName = $('#inputSong').val();
		let singerName = $('#inputSinger').val();  		
	
		let singerId = "";

		try{
		  singerId = Singers.findOne({name: singerName})._id;
		}
		catch(ex){
		  singerId = Singers.insert({name: singerName});
		}
		// if(singerId === undefined){
		//   singerId = Singers.insert({name: singerName});
		// }		
		 
		 let id = new Mongo.ObjectID(instance.playlistId);
		 let songId = Songs.insert({title:songName,singer:singerId});
		 Playlists.update({_id: id}, {$push: {song: songId}});
		 //$('#addModal').hide();
     return false;

  },

	'click .btn-success':(event,instance) => {	
		instance.playlistId = event.target.name;
	}
});
