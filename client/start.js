Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
  Session.set("counter",0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  }
});

Template.hello.events({
  'click button':(event, instance)=> {
    // increment the counter when button is clicked

    event.target.innerHTML = event.target.innerHTML.split('').reverse().join('');
    Session.set("counter", Session.get("counter")+1);
    //instance.counter.set(instance.counter.get() + 1);
    instance.counter.set(Session.get("counter"));
  },
});