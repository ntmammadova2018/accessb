Router.route('/start', function(){
	this.render('hello');
});

Router.route('/', function(){
	this.render('playlists');
});

Router.route('/onePlaylist/:id', {

  waitOn: function () {
    const playlistId = this.params.id;
    let id = new Mongo.ObjectID(playlistId);
	Meteor.subscribe("playlistSongs", id);
	Meteor.subscribe("specificSingers", id);
	Meteor.subscribe("specificSongs", id);
  },

  action: function () {
    this.render('onePlaylist', {data: this.params.id});
  }
});