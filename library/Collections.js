Songs = new Mongo.Collection("songs");
Singers = new Mongo.Collection("singers");
Playlists = new Mongo.Collection("playlists");

Singers.Schema = new SimpleSchema({
  name: {
  	type: String,
    label: "Name of the singer"
	}  
});

Songs.Schema = new SimpleSchema({
  title: {
  	type: String,
    label: "Name of the song"
	},
  singer: {
  	type: String,
	  label: "ID of the Singer"
	},
  album:{
  	type: String,
  	label: "Album of the song",
  	defaultValue: "Unknown"
	}  
});

Playlists.Schema = new SimpleSchema({
  name: {
  	type: String,
	  label: "Name of the playlist"
	},
  userId:{
    type: Meteor.user,
    label: "User who owns the playlist"
  },
  song: {
  	type: Array,  	
	  label: "Array of songs' Ids"
	},
  "song.$": {
  	type: String,
  	label: "Element of Array"
  }
});

// Songs.attachSchema(songSchema);
// Singers.attachSchema(singerSchema);
// Playlists.attachSchema(playlistSchema);